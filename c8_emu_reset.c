#include "c8.h"

int
c8_emu_reset(C8_Emu *const emu)
{
	unsigned int i;
	if (emu == NULL)
		return -C8_ERR_ARG_NULL;
	for (i = 0; i < C8_EMU_STK_SIZE; ++i)
		emu->stk[i] = 0x0;
	emu->ireg = 0x0;
	emu->pc = C8_EMU_PRG_START;
	for (i = 0; i < C8_EMU_REG_SIZE; ++i)
		emu->vreg[i] = 0x0;
	emu->dtm = 0x0;
	emu->stm = 0x0;
	emu->sp = 0;
	return C8_ERR_NONE;
}

