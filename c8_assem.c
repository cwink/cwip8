#include "c8.h"

#define C8_OPC_BLD_ANNN(A, NNN)     (((((A) & 0xFU) << 12U) & 0xF000U) | (NNN & 0xFFFU))
#define C8_OPC_BLD_AXNN(A, X, NN)   (((((A) & 0xFU) << 12U) & 0xF000U) | ((((X) & 0xFU) << 8U) & 0xF00U) | ((NN) & 0xFFU))
#define C8_OPC_BLD_AXYN(A, X, Y, N) (((((A) & 0xFU) << 12U) & 0xF000U) | ((((X) & 0xFU) << 8U) & 0xF00U) | ((((Y) & 0xFU) << 4U) & 0xF0U) | ((N) & 0xFU))

#define C8_IS_VREG(PARAM) ((PARAM)[0] == C8_SYM_VREG[0])

static int
c8_get_num(const char *const param)
{
	int v, ec;
	if ((ec = sscanf(param, "%i", &v)) == 0 || ec == EOF || v < 0) {
		return -1;
	}
	return v;
}

static int
c8_get_const(const char *const param)
{
	int v;
	if ((v = c8_get_num(param)) < 0 || v > 0xFFF)
		return -1;
	return v;
}

static int
c8_get_byte(const char *const param)
{
	int v;
	if ((v = c8_get_num(param)) < 0 || v > 0xFF)
		return -1;
	return v;
}

static int
c8_get_nib(const char *const param)
{
	int v;
	if ((v = c8_get_num(param)) < 0 || v > 0xF)
		return -1;
	return v;
}

static int
c8_get_vreg(const char *const param)
{
	if (param[2] != '\0')
		return -1;
	if (param[1] >= '0' && param[1] <= '9')
		return param[1] - '0';
	if (param[1] >= 'a' && param[1] <= 'f')
		return 10 + (param[1] - 'a');
	if (param[1] >= 'A' && param[1] <= 'F')
		return 10 + (param[1] - 'A');
	return -1;
}

static int
c8_cmd_add(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	unsigned int a, b;
	int v;
	if (length != 2)
		return -C8_ERR_NUM_PARMS;
	if (C8_IS_VREG(params[0])) {
		if ((v = c8_get_vreg(params[0])) < 0)
			return -C8_ERR_INV_PARMS;
		a = (unsigned int)v;
		if (C8_IS_VREG(params[1])) {
			if ((v = c8_get_vreg(params[1])) < 0)
				return -C8_ERR_INV_PARMS;
			b = (unsigned int)v;
			*opc = C8_OPC_BLD_AXYN(0x8, a, b, 4);
		} else {
			if ((v = c8_get_byte(params[1])) < 0)
				return -C8_ERR_INV_PARMS;
			b = (unsigned int)v;
			*opc = C8_OPC_BLD_AXNN(0x7, a, b);
		}
	} else if (C8_STRCMP(params[0], C8_SYM_IREG) == 0) {
		if ((v = c8_get_vreg(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		b = (unsigned int)v;
		*opc = C8_OPC_BLD_AXNN(0xF, b, 0x1E);
	} else {
		return -C8_ERR_INV_PARMS;
	}
	return C8_ERR_NONE;
}

static int
c8_cmd_and(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	unsigned int a, b;
	int v;
	if (length != 2)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	a = (unsigned int)v;
	if ((v = c8_get_vreg(params[1])) < 0)
		return -C8_ERR_INV_PARMS;
	b = (unsigned int)v;
	*opc = C8_OPC_BLD_AXYN(0x8, a, b, 0x2);
	return C8_ERR_NONE;
}

static int
c8_cmd_call(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	int v;
	if (length != 1)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_const(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	*opc = C8_OPC_BLD_ANNN(0x2, v);
	return C8_ERR_NONE;
}

static int
c8_cmd_cls(const unsigned long length, unsigned int *const opc)
{
	if (length != 0)
		return -C8_ERR_NUM_PARMS;
	*opc = 0x00E0;
	return C8_ERR_NONE;
}

static int
c8_cmd_drw(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	unsigned int a, b, c;
	int v;
	if (length != 3)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	a = (unsigned int)v;
	if ((v = c8_get_vreg(params[1])) < 0)
		return -C8_ERR_INV_PARMS;
	b = (unsigned int)v;
	if ((v = c8_get_nib(params[2])) < 0)
		return -C8_ERR_INV_PARMS;
	c = (unsigned int)v;
	*opc = C8_OPC_BLD_AXYN(0xD, a, b, c);
	return C8_ERR_NONE;
}

static int
c8_cmd_jmp(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	int v;
	if (length < 1 || length > 2)
		return -C8_ERR_NUM_PARMS;
	if (length == 1) {
		if ((v = c8_get_const(params[0])) < 0)
			return -C8_ERR_INV_PARMS;
		*opc = C8_OPC_BLD_ANNN(0x1, (unsigned int)v);
	} else {
		if (!(C8_IS_VREG(params[0]) && params[0][1] == '0') || (v = c8_get_const(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		*opc = C8_OPC_BLD_ANNN(0xB, (unsigned int)v);
	}
	return C8_ERR_NONE;
}

static int
c8_cmd_mov(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	unsigned int a, b;
	int v;
	if (length != 2)
		return -C8_ERR_NUM_PARMS;
	if (C8_IS_VREG(params[0])) {
		if ((v = c8_get_vreg(params[0])) < 0)
			return -C8_ERR_INV_PARMS;
		a = (unsigned int)v;
		if (C8_IS_VREG(params[1])) {
			if ((v = c8_get_vreg(params[1])) < 0)
				return -C8_ERR_INV_PARMS;
			b = (unsigned int)v;
			*opc = C8_OPC_BLD_AXYN(0x8, a, b, 0x0);
		} else if (C8_STRCMP(params[1], C8_SYM_DTM) == 0) {
			*opc = C8_OPC_BLD_AXNN(0xF, a, 0x07);
		} else if (C8_STRCMP(params[1], C8_SYM_KEY) == 0) {
			*opc = C8_OPC_BLD_AXNN(0xF, a, 0x0A);
		} else if (C8_STRCMP(params[1], C8_SYM_IMEM) == 0) {
			*opc = C8_OPC_BLD_AXNN(0xF, a, 0x65);
		} else {
			if ((v = c8_get_const(params[1])) < 0)
				return -C8_ERR_INV_PARMS;
			b = (unsigned int)v;
			*opc = C8_OPC_BLD_AXNN(0x6, a, b);
		}
	} else if (C8_STRCMP(params[0], C8_SYM_IREG) == 0) {
		if ((v = c8_get_const(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		*opc = C8_OPC_BLD_ANNN(0xA, (unsigned int)v);
	} else if (C8_STRCMP(params[0], C8_SYM_DTM) == 0) {
		if ((v = c8_get_vreg(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		*opc = C8_OPC_BLD_AXNN(0xF, (unsigned int)v, 0x15);
	} else if (C8_STRCMP(params[0], C8_SYM_STM) == 0) {
		if ((v = c8_get_vreg(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		*opc = C8_OPC_BLD_AXNN(0xF, (unsigned int)v, 0x18);
	} else if (C8_STRCMP(params[0], C8_SYM_FONT) == 0) {
		if ((v = c8_get_vreg(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		*opc = C8_OPC_BLD_AXNN(0xF, (unsigned int)v, 0x29);
	} else if (C8_STRCMP(params[0], C8_SYM_BCD) == 0) {
		if ((v = c8_get_vreg(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		*opc = C8_OPC_BLD_AXNN(0xF, (unsigned int)v, 0x33);
	} else if (C8_STRCMP(params[0], C8_SYM_IMEM) == 0) {
		if ((v = c8_get_vreg(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		*opc = C8_OPC_BLD_AXNN(0xF, (unsigned int)v, 0x55);
	} else {
		return -C8_ERR_INV_PARMS;
	}
	return C8_ERR_NONE;
}

static int
c8_cmd_or(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	unsigned int a, b;
	int v;
	if (length != 2)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	a = (unsigned int)v;
	if ((v = c8_get_vreg(params[1])) < 0)
		return -C8_ERR_INV_PARMS;
	b = (unsigned int)v;
	*opc = C8_OPC_BLD_AXYN(0x8, a, b, 0x1);
	return C8_ERR_NONE;
}

static int
c8_cmd_ret(const unsigned long length, unsigned int *const opc)
{
	if (length != 0)
		return -C8_ERR_NUM_PARMS;
	*opc = 0x00EE;
	return C8_ERR_NONE;
}

static int
c8_cmd_rnd(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	unsigned int a, b;
	int v;
	if (length != 2)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	a = (unsigned int)v;
	if ((v = c8_get_byte(params[1])) < 0)
		return -C8_ERR_INV_PARMS;
	b = (unsigned int)v;
	*opc = C8_OPC_BLD_AXNN(0xC, a, b);
	return C8_ERR_NONE;
}

static int
c8_cmd_shl(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	int v;
	if (length != 1)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	*opc = C8_OPC_BLD_AXNN(0x8, (unsigned int)v, 0x0E);
	return C8_ERR_NONE;
}

static int
c8_cmd_shr(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	int v;
	if (length != 1)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	*opc = C8_OPC_BLD_AXNN(0x8, (unsigned int)v, 0x06);
	return C8_ERR_NONE;
}

static int
c8_cmd_se(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	unsigned int a, b;
	int v;
	if (length != 2)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	a = (unsigned int)v;
	if (C8_IS_VREG(params[1])) {
		if ((v = c8_get_vreg(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		b = (unsigned int)v;
		*opc = C8_OPC_BLD_AXYN(0x5, a, b, 0x0);
	} else {
		if ((v = c8_get_const(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		b = (unsigned int)v;
		*opc = C8_OPC_BLD_AXNN(0x3, a, b);
	}
	return C8_ERR_NONE;
}

static int
c8_cmd_sne(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	unsigned int a, b;
	int v;
	if (length != 2)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	a = (unsigned int)v;
	if (C8_IS_VREG(params[1])) {
		if ((v = c8_get_vreg(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		b = (unsigned int)v;
		*opc = C8_OPC_BLD_AXYN(0x9, a, b, 0x0);
	} else {
		if ((v = c8_get_const(params[1])) < 0)
			return -C8_ERR_INV_PARMS;
		b = (unsigned int)v;
		*opc = C8_OPC_BLD_AXNN(0x4, a, b);
	}
	return C8_ERR_NONE;
}

static int
c8_cmd_sknp(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	int v;
	if (length != 1)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	*opc = C8_OPC_BLD_AXNN(0xE, (unsigned int)v, 0xA1);
	return C8_ERR_NONE;
}

static int
c8_cmd_skp(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	int v;
	if (length != 1)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	*opc = C8_OPC_BLD_AXNN(0xE, (unsigned int)v, 0x9E);
	return C8_ERR_NONE;
}

static int
c8_cmd_sub(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	unsigned int a, b;
	int v;
	if (length != 2)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	a = (unsigned int)v;
	if ((v = c8_get_vreg(params[1])) < 0)
		return -C8_ERR_INV_PARMS;
	b = (unsigned int)v;
	*opc = C8_OPC_BLD_AXYN(0x8, a, b, 0x5);
	return C8_ERR_NONE;
}

static int
c8_cmd_subn(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	unsigned int a, b;
	int v;
	if (length != 2)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	a = (unsigned int)v;
	if ((v = c8_get_vreg(params[1])) < 0)
		return -C8_ERR_INV_PARMS;
	b = (unsigned int)v;
	*opc = C8_OPC_BLD_AXYN(0x8, a, b, 0x7);
	return C8_ERR_NONE;
}

static int
c8_cmd_sys(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	int v;
	if (length != 1)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_const(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	*opc = C8_OPC_BLD_ANNN(0x0, (unsigned int)v);
	return C8_ERR_NONE;
}

static int
c8_cmd_xor(char *const *const params, const unsigned long length, unsigned int *const opc)
{
	unsigned int a, b;
	int v;
	if (length != 2)
		return -C8_ERR_NUM_PARMS;
	if ((v = c8_get_vreg(params[0])) < 0)
		return -C8_ERR_INV_PARMS;
	a = (unsigned int)v;
	if ((v = c8_get_vreg(params[1])) < 0)
		return -C8_ERR_INV_PARMS;
	b = (unsigned int)v;
	*opc = C8_OPC_BLD_AXYN(0x8, a, b, 0x3);
	return C8_ERR_NONE;
}

static int
c8_process(const char *const cmd, char *const *const params, const unsigned long length, unsigned int *const opc)
{
	int ec;
	if (C8_STRCMP(cmd, "ADD") == 0) {
		ec = c8_cmd_add(params, length, opc);
	} else if (C8_STRCMP(cmd, "AND") == 0) {
		ec = c8_cmd_and(params, length, opc);
	} else if (C8_STRCMP(cmd, "CALL") == 0) {
		ec = c8_cmd_call(params, length, opc);
	} else if (C8_STRCMP(cmd, "CLS") == 0) {
		ec = c8_cmd_cls(length, opc);
	} else if (C8_STRCMP(cmd, "DRW") == 0) {
		ec = c8_cmd_drw(params, length, opc);
	} else if (C8_STRCMP(cmd, "JMP") == 0) {
		ec = c8_cmd_jmp(params, length, opc);
	} else if (C8_STRCMP(cmd, "MOV") == 0) {
		ec = c8_cmd_mov(params, length, opc);
	} else if (C8_STRCMP(cmd, "OR") == 0) {
		ec = c8_cmd_or(params, length, opc);
	} else if (C8_STRCMP(cmd, "RET") == 0) {
		ec = c8_cmd_ret(length, opc);
	} else if (C8_STRCMP(cmd, "RND") == 0) {
		ec = c8_cmd_rnd(params, length, opc);
	} else if (C8_STRCMP(cmd, "SHL") == 0) {
		ec = c8_cmd_shl(params, length, opc);
	} else if (C8_STRCMP(cmd, "SHR") == 0) {
		ec = c8_cmd_shr(params, length, opc);
	} else if (C8_STRCMP(cmd, "SE") == 0) {
		ec = c8_cmd_se(params, length, opc);
	} else if (C8_STRCMP(cmd, "SNE") == 0) {
		ec = c8_cmd_sne(params, length, opc);
	} else if (C8_STRCMP(cmd, "SKNP") == 0) {
		ec = c8_cmd_sknp(params, length, opc);
	} else if (C8_STRCMP(cmd, "SKP") == 0) {
		ec = c8_cmd_skp(params, length, opc);
	} else if (C8_STRCMP(cmd, "SUB") == 0) {
		ec = c8_cmd_sub(params, length, opc);
	} else if (C8_STRCMP(cmd, "SUBN") == 0) {
		ec = c8_cmd_subn(params, length, opc);
	} else if (C8_STRCMP(cmd, "SYS") == 0) {
		ec = c8_cmd_sys(params, length, opc);
	} else if (C8_STRCMP(cmd, "XOR") == 0) {
		ec = c8_cmd_xor(params, length, opc);
	} else {
		ec = -C8_ERR_INV_CMD;
	}
	return ec;
}

int
c8_assem(char *buffer, unsigned char opcodes[2])
{
	unsigned int p_len, opc;
	int ec;
	char *params[3], *ptr, *end, *cmd;
	if (buffer == NULL)
		return -C8_ERR_INV_BUFFER;
	for (; *buffer == ' ' || *buffer == '\t'; ++buffer)
		;
	if (*buffer == '\0')
		return -C8_ERR_INV_BUFFER;
	for (ptr = buffer; *ptr != '\0'; ++ptr)
		if (*ptr == ' ' || *ptr == '\t')
			*ptr = '\0';
	end = ptr;
	cmd = ptr = buffer;
	for (; *ptr != '\0'; ++ptr)
		;
	p_len = 0;
	params[0] = params[1] = params[2] = NULL;
	for (; ptr != end; ++ptr) {
		if (*ptr == ',') {
			*ptr = '\0';
			++p_len;
			continue;
		}
		if (*ptr != '\0' && params[p_len] == NULL)
			params[p_len] = ptr;
	}
	if (params[0] != NULL)
		++p_len;
	if ((ec = c8_process(cmd, params, p_len, &opc)) < 0)
		return ec;
	opcodes[0] = (opc & 0xFF00U) >> 8U;
	opcodes[1] = opc & 0xFFU;
	return C8_ERR_NONE;
}
