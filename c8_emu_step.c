#include "c8.h"

static void
c8_emu_draw(C8_Emu *const emu, const unsigned int vx, const unsigned int vy, const unsigned int n)
{
	unsigned int x, y, xx, yy, i, ii;
	i = emu->ireg;
	emu->vreg[0xF] = 0x0;
	for (y = 0; y < n; ++y) {
		ii = emu->ram[i];
		yy = (vy + y) % C8_EMU_GFX_HEIGHT;
		for (x = 0; x < 8; ++x) {
			xx = (vx + x) % C8_EMU_GFX_WIDTH;
			if ((ii & 0x80U) == 0x80U) {
				if (emu->cbs.pixel_get(xx, yy)) {
					emu->cbs.pixel_set(xx, yy, 0);
					emu->vreg[0xF] = 0x1;
				} else {
					emu->cbs.pixel_set(xx, yy, 1);
				}
			}
			ii = (ii & 0x7FU) << 1U;
		}
		++i;
	}
}

int
c8_emu_step(C8_Emu *const emu)
{
	unsigned int v;
	unsigned char c;
	const unsigned int opcode = ((emu->ram[emu->pc] << 8U) & 0xFF00U) | (emu->ram[emu->pc + 1] & 0xFFU);
	switch (C8_OPC_GET_HH(opcode)) {
	case 0x0:
		switch (C8_OPC_GET_L(opcode)) {
		case 0xE0:
			emu->cbs.clr_scr();
			emu->pc += 2;
			break;
		case 0xEE:
			emu->pc = emu->stk[emu->sp--] + 2;
			break;
		default:
			emu->pc += 2;
			break;
		}
		break;
	case 0x1:
		emu->pc = C8_OPC_GET_NNN(opcode);
		break;
	case 0x2:
		emu->stk[++emu->sp] = emu->pc;
		emu->pc = C8_OPC_GET_NNN(opcode);
		break;
	case 0x3:
		if (emu->vreg[C8_OPC_GET_X(opcode)] == C8_OPC_GET_NN(opcode))
			emu->pc += 2;
		emu->pc += 2;
		break;
	case 0x4:
		if (emu->vreg[C8_OPC_GET_X(opcode)] != C8_OPC_GET_NN(opcode))
			emu->pc += 2;
		emu->pc += 2;
		break;
	case 0x5:
		if (C8_OPC_GET_LL(opcode) != 0x0) {
			return -C8_ERR_INV_OPC;
		}
		if (emu->vreg[C8_OPC_GET_X(opcode)] == emu->vreg[C8_OPC_GET_Y(opcode)])
			emu->pc += 2;
		emu->pc += 2;
		break;
	case 0x6:
		emu->vreg[C8_OPC_GET_X(opcode)] = C8_OPC_GET_NN(opcode);
		emu->pc += 2;
		break;
	case 0x7:
		emu->vreg[C8_OPC_GET_X(opcode)] += C8_OPC_GET_NN(opcode);
		emu->pc += 2;
		break;
	case 0x8:
		switch (C8_OPC_GET_LL(opcode)) {
		case 0x0:
			emu->vreg[C8_OPC_GET_X(opcode)] = emu->vreg[C8_OPC_GET_Y(opcode)];
			emu->pc += 2;
			break;
		case 0x1:
			emu->vreg[C8_OPC_GET_X(opcode)] |= emu->vreg[C8_OPC_GET_Y(opcode)];
			emu->pc += 2;
			break;
		case 0x2:
			emu->vreg[C8_OPC_GET_X(opcode)] &= emu->vreg[C8_OPC_GET_Y(opcode)];
			emu->pc += 2;
			break;
		case 0x3:
			emu->vreg[C8_OPC_GET_X(opcode)] ^= emu->vreg[C8_OPC_GET_Y(opcode)];
			emu->pc += 2;
			break;
		case 0x4:
			v = (unsigned int)emu->vreg[C8_OPC_GET_X(opcode)] + (unsigned int)emu->vreg[C8_OPC_GET_Y(opcode)];
			emu->vreg[0xF] = v > 0xFFU;
			emu->vreg[C8_OPC_GET_X(opcode)] = v & 0xFFU;
			emu->pc += 2;
			break;
		case 0x5:
			emu->vreg[0xF] = emu->vreg[C8_OPC_GET_X(opcode)] > emu->vreg[C8_OPC_GET_Y(opcode)];
			emu->vreg[C8_OPC_GET_X(opcode)] -= emu->vreg[C8_OPC_GET_Y(opcode)];
			emu->pc += 2;
			break;
		case 0x6:
			emu->vreg[0xF] = (emu->vreg[C8_OPC_GET_X(opcode)] & 0x1U) == 0x1U;
			emu->vreg[C8_OPC_GET_X(opcode)] >>= 1U;
			emu->pc += 2;
			break;
		case 0x7:
			emu->vreg[0xF] = emu->vreg[C8_OPC_GET_Y(opcode)] > emu->vreg[C8_OPC_GET_X(opcode)];
			emu->vreg[C8_OPC_GET_X(opcode)] = emu->vreg[C8_OPC_GET_Y(opcode)] - emu->vreg[C8_OPC_GET_X(opcode)];
			emu->pc += 2;
			break;
		case 0xE:
			emu->vreg[0xF] = (emu->vreg[C8_OPC_GET_X(opcode)] & 0x80U) == 0x80U;
			emu->vreg[C8_OPC_GET_X(opcode)] <<= 1U;
			emu->pc += 2;
			break;
		default:
			return -C8_ERR_INV_OPC;
		}
		break;
	case 0x9:
		if (C8_OPC_GET_LL(opcode) != 0x0) {
			return -C8_ERR_INV_OPC;
		}
		if (emu->vreg[C8_OPC_GET_X(opcode)] != emu->vreg[C8_OPC_GET_Y(opcode)])
			emu->pc += 2;
		emu->pc += 2;
		break;
	case 0xA:
		emu->ireg = C8_OPC_GET_NNN(opcode);
		emu->pc += 2;
		break;
	case 0xB:
		emu->pc = emu->vreg[0] + C8_OPC_GET_NNN(opcode);
		break;
	case 0xC:
		emu->vreg[C8_OPC_GET_X(opcode)] = emu->cbs.random() & C8_OPC_GET_NN(opcode);
		emu->pc += 2;
		break;
	case 0xD:
		c8_emu_draw(emu, emu->vreg[C8_OPC_GET_X(opcode)], emu->vreg[C8_OPC_GET_Y(opcode)], C8_OPC_GET_N(opcode));
		emu->pc += 2;
		break;
	case 0xE:
		switch (C8_OPC_GET_L(opcode)) {
		case 0x9E:
			if (emu->cbs.key_press(emu->vreg[C8_OPC_GET_X(opcode)]))
				emu->pc += 2;
			emu->pc += 2;
			break;
		case 0xA1:
			if (!emu->cbs.key_press(emu->vreg[C8_OPC_GET_X(opcode)]))
				emu->pc += 2;
			emu->pc += 2;
			break;
		default:
			return -C8_ERR_INV_OPC;
		}
		break;
	case 0xF:
		switch (C8_OPC_GET_L(opcode)) {
		case 0x07:
			emu->vreg[C8_OPC_GET_X(opcode)] = emu->dtm;
			emu->pc += 2;
			break;
		case 0x0A:
			c = emu->cbs.get_key();
			if (c > C8_EMU_KEY_MAX)
				return -C8_ERR_INV_KEY;
			emu->vreg[C8_OPC_GET_X(opcode)] = c;
			emu->pc += 2;
			break;
		case 0x15:
			emu->dtm = emu->vreg[C8_OPC_GET_X(opcode)];
			emu->pc += 2;
			break;
		case 0x18:
			emu->stm = emu->vreg[C8_OPC_GET_X(opcode)];
			emu->pc += 2;
			break;
		case 0x1E:
			emu->ireg += emu->vreg[C8_OPC_GET_X(opcode)];
			emu->pc += 2;
			break;
		case 0x29:
			emu->ireg = 5 * emu->vreg[C8_OPC_GET_X(opcode)];
			emu->pc += 2;
			break;
		case 0x33:
			emu->ram[emu->ireg] = emu->vreg[C8_OPC_GET_X(opcode)] / 100;
			emu->ram[emu->ireg + 1] = (emu->vreg[C8_OPC_GET_X(opcode)] / 10) % 10;
			emu->ram[emu->ireg + 2] = (emu->vreg[C8_OPC_GET_X(opcode)] % 100) % 10;
			emu->pc += 2;
			break;
		case 0x55:
			for (v = 0; v <= C8_OPC_GET_X(opcode); ++v)
				emu->ram[emu->ireg + v] = emu->vreg[v];
			emu->pc += 2;
			break;
		case 0x65:
			for (v = 0; v <= C8_OPC_GET_X(opcode); ++v)
				emu->vreg[v] = emu->ram[emu->ireg + v];
			emu->pc += 2;
			break;
		default:
			return -C8_ERR_INV_OPC;
		}
		break;
	default:
		return -C8_ERR_INV_OPC;
	}
	return C8_ERR_NONE;
}

