.POSIX:
.SUFFIXES:

CC      = gcc
CFLAGS  = -std=c99 -pedantic-errors -Wall -Wextra -g -Og -D_POSIX_C_SOURCE=200809L
LDFLAGS =
LDLIBS  =

all: disasm.x assem.x emu.x

disasm.x: c8_disasm.o
assem.x: c8_assem.o

emu.x: emu.o c8_emu_init.o c8_emu_reset.o c8_emu_step.o c8_emu_timers.o
	$(CC) $(LDFLAGS) -o $@ $^ `sdl2-config --libs`
emu.o: emu.c
	$(CC) $(CFLAGS) `sdl2-config --cflags` -c -o $@ $<

run_disasm: disasm.x
	cat pong.ch8 | ./disasm.x
run_assem: assem.x
	cat pong.csm | ./assem.x | hexdump
run_emu: emu.x
	cat pong.ch8 | ./emu.x
clean:
	rm -rf *.o *.x

.SUFFIXES: .c .o .x
.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<
.o.x:
	$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)
