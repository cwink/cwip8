#ifdef _WIN32
#	include <fcntl.h>
#	include <io.h>
#endif /* _WIN32 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <SDL2/SDL.h>

#include "c8.h"

#define WINDOW_SCALE 10
#define CLOCK_HZ     (1.0 / 700.0)
#define TIMER_HZ     (1.0 / 60.0)

int  sdl_init(void);
void sdl_term(void);

SDL_Window   *window;
SDL_Renderer *render;
SDL_Texture  *text;

static void
clr_scr(void)
{
	void *pix;
	unsigned int i;
	int pit;
	/* FIXME: Add error handling to this function. */
	SDL_LockTexture(text, NULL, &pix, &pit);
	for (i = 0; i < C8_EMU_GFX_WIDTH * C8_EMU_GFX_HEIGHT; ++i)
		((Uint32 *)pix)[i] = 0x000000FFU;
	SDL_UnlockTexture(text);
}

static void
pixel_set(unsigned int x, unsigned int y, int on)
{
	void *pix;
	int pit;
	/* FIXME: Add error handling to this function. */
	SDL_LockTexture(text, NULL, &pix, &pit);
	((Uint32 *)pix)[(y * C8_EMU_GFX_WIDTH) + x] = on ? 0xFFFFFFFFU : 0x000000FFU;
	SDL_UnlockTexture(text);
}

static int
pixel_get(unsigned int x, unsigned int y)
{
	int o;
	void *pix;
	int pit;
	/* FIXME: Add error handling to this function. */
	SDL_LockTexture(text, NULL, &pix, &pit);
	o = ((Uint32 *)pix)[(y * C8_EMU_GFX_WIDTH) + x] == 0xFFFFFFFFU;
	SDL_UnlockTexture(text);
	return o;
}

static int
key_press(const unsigned char key)
{
	switch (key) {
	case 0x0:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_0];
	case 0x1:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_1];
	case 0x2:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_2];
	case 0x3:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_3];
	case 0x4:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_4];
	case 0x5:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_5];
	case 0x6:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_6];
	case 0x7:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_7];
	case 0x8:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_8];
	case 0x9:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_9];
	case 0xA:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_A];
	case 0xB:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_B];
	case 0xC:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_C];
	case 0xD:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_D];
	case 0xE:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_E];
	case 0xF:
		return SDL_GetKeyboardState(NULL)[SDL_SCANCODE_F];
	}
	return 0;
}

static unsigned char
get_key(void)
{
	SDL_Event ev;
	unsigned char key;
	while (1) {
		while (SDL_PollEvent(&ev)) {
			if (ev.key.type == SDL_KEYDOWN &&
			    ev.key.state ==SDL_PRESSED) {
				switch (ev.key.keysym.scancode) {
				case SDL_SCANCODE_0:
					key = 0x0;
					break;
				case SDL_SCANCODE_1:
					key = 0x1;
					break;
				case SDL_SCANCODE_2:
					key = 0x2;
					break;
				case SDL_SCANCODE_3:
					key = 0x3;
					break;
				case SDL_SCANCODE_4:
					key = 0x4;
					break;
				case SDL_SCANCODE_5:
					key = 0x5;
					break;
				case SDL_SCANCODE_6:
					key = 0x6;
					break;
				case SDL_SCANCODE_7:
					key = 0x7;
					break;
				case SDL_SCANCODE_8:
					key = 0x8;
					break;
				case SDL_SCANCODE_9:
					key = 0x9;
					break;
				case SDL_SCANCODE_A:
					key = 0xA;
					break;
				case SDL_SCANCODE_B:
					key = 0xB;
					break;
				case SDL_SCANCODE_C:
					key = 0xC;
					break;
				case SDL_SCANCODE_D:
					key = 0xD;
					break;
				case SDL_SCANCODE_E:
					key = 0xE;
					break;
				case SDL_SCANCODE_F:
					key = 0xF;
					break;
				default:
					continue;
				}
				goto finish;
			}
		}
	}
finish:
	return key;
}

static unsigned char
random(void)
{
	return rand() % 0xFFU;
}

int
sdl_init(void)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
		return -1;
	if ((window = SDL_CreateWindow("SDL2", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, C8_EMU_GFX_WIDTH * WINDOW_SCALE, C8_EMU_GFX_HEIGHT * WINDOW_SCALE, 0)) == NULL)
		return -1;
	if ((render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC)) == NULL)
		return -1;
	if (SDL_RenderSetLogicalSize(render, C8_EMU_GFX_WIDTH, C8_EMU_GFX_HEIGHT) < 0)
		return -1;
	if ((text = SDL_CreateTexture(render, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, C8_EMU_GFX_WIDTH, C8_EMU_GFX_HEIGHT)) == NULL)
		return -1;
	return 0;
}

int
sdl_render(void)
{
	if (SDL_RenderCopy(render, text, NULL, NULL) < 0)
		return -1;
	SDL_RenderPresent(render);
	return 0;
}

void
sdl_term(void)
{
	SDL_DestroyTexture(text);
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

int
main(int argc, char *argv[])
{
	Uint32 c_time, l_time;
	double d_time, t_accum, c_accum;
	SDL_Event ev;
	C8_Emu emu;
	unsigned int p_len;
	int ch;
	unsigned char prog[C8_EMU_PRG_MAX_SZ];
	const C8_CBacks cbs = { clr_scr, pixel_set, pixel_get, key_press, get_key, random };
	srand(time(NULL));
#ifdef _WIN32
	if (_setmode(_fileno(stdin), _O_BINARY) == -1) {
		fprintf(stderr, "ERROR: fialed to set stdin to binary mode.\n");
		return EXIT_FAILURE;
	}
#endif /* _WIN32 */
	for (p_len = 0; (ch = fgetc(stdin)) != EOF; ++p_len)
		prog[p_len] = (unsigned char)ch;
	if (ferror(stdin) != 0) {
		fprintf(stderr, "ERROR: failed to read data from stdin.\n");
		return EXIT_FAILURE;
	}
	if (sdl_init() < 0) {
		fprintf(stderr, "ERROR: %s\n", SDL_GetError());
		return EXIT_FAILURE;
	}
	clr_scr();
	c8_emu_init(&emu, prog, p_len, &cbs);
	l_time = SDL_GetTicks();
	t_accum = c_accum = 0.0;
	while (1) {
		c_time = SDL_GetTicks();
		d_time = ((double)c_time / 1000.0) - ((double)l_time / 1000.0);
		if (d_time > 1.0) /* XXX: Cap's at 1 second. */
			d_time = 1.0;
		l_time = c_time;
		t_accum += d_time;
		c_accum += d_time;
		while (SDL_PollEvent(&ev))
			if (ev.type == SDL_WINDOWEVENT && ev.window.event == SDL_WINDOWEVENT_CLOSE)
				goto finish;
		while (c_accum >= CLOCK_HZ) {
			c8_emu_step(&emu);
			c_accum -= CLOCK_HZ;
		}
		while (t_accum >= TIMER_HZ) {
			c8_emu_timers(&emu);
			t_accum -= TIMER_HZ;
		}
		if (sdl_render() < 0) {
			fprintf(stderr, "ERROR: %s\n", SDL_GetError());
			return EXIT_FAILURE;
		}
	}
finish:
	sdl_term();
	return EXIT_SUCCESS;
}
