#ifndef C8_H
#define C8_H

#ifndef NULL
#	ifdef __cplusplus
#		include <cstddef>
#	else
#		include <stddef.h>
#	endif
#endif /* NULL */

#ifndef C8_SPRINTF
#	ifdef __cplusplus
#		include <cstdio>
#		define C8_SPRINTF std::sprintf
#	else
#		include <stdio.h>
#		define C8_SPRINTF sprintf
#	endif
#endif /* C8_SPRINTF */

#ifndef C8_STRCMP
#	ifdef __cplusplus
#		include <cstring>
#		define C8_STRCMP std::strcmp
#	else
#		include <string.h>
#		define C8_STRCMP strcmp
#	endif
#endif /* C8_STRCMP */

#ifndef C8_SSCANF
#	ifdef __cplusplus
#		include <cstdio>
#		define C8_SSCANF std::sscanf
#	else
#		include <stdio.h>
#		define C8_SSCANF sscanf
#	endif
#endif /* C8_SSCANF */

#define C8_ERR_NONE       0
#define C8_ERR_INV_OPC    1
#define C8_ERR_INV_BUFFER 2
#define C8_ERR_INV_CMD    3
#define C8_ERR_NUM_PARMS  4
#define C8_ERR_INV_PARMS  5
#define C8_ERR_ARG_NULL   6
#define C8_ERR_PRG_SIZE   7
#define C8_ERR_INV_KEY    8

#define C8_SYM_ADD  "ADD"
#define C8_SYM_AND  "AND"
#define C8_SYM_CALL "CALL"
#define C8_SYM_CLS  "CLS"
#define C8_SYM_DRW  "DRW"
#define C8_SYM_JMP  "JMP"
#define C8_SYM_MOV  "MOV"
#define C8_SYM_OR   "OR"
#define C8_SYM_RET  "RET"
#define C8_SYM_RND  "RND"
#define C8_SYM_SHL  "SHL"
#define C8_SYM_SHR  "SHR"
#define C8_SYM_SE   "SE"
#define C8_SYM_SNE  "SNE"
#define C8_SYM_SKNP "SKNP"
#define C8_SYM_SKP  "SKP"
#define C8_SYM_SUB  "SUB"
#define C8_SYM_SUBN "SUBN"
#define C8_SYM_SYS  "SYS"
#define C8_SYM_XOR  "XOR"
#define C8_SYM_IMEM "[I]"
#define C8_SYM_IREG "I"
#define C8_SYM_VREG "V"
#define C8_SYM_DTM  "DT"
#define C8_SYM_STM  "ST"
#define C8_SYM_KEY  "K"
#define C8_SYM_FONT "F"
#define C8_SYM_BCD  "B"

#define C8_EMU_RAM_SIZE   4096
#define C8_EMU_PRG_START  512
#define C8_EMU_PRG_MAX_SZ (C8_EMU_RAM_SIZE - C8_EMU_PRG_START)
#define C8_EMU_REG_SIZE   16
#define C8_EMU_STK_SIZE   16
#define C8_EMU_KEY_MAX    0xF
#define C8_EMU_GFX_WIDTH  64
#define C8_EMU_GFX_HEIGHT 32

#define C8_OPC_GET_HH(OPCODE)  ((((OPCODE) & 0xF000U) >> 12U) & 0xFU)
#define C8_OPC_GET_L(OPCODE)   ((OPCODE) & 0xFFU)
#define C8_OPC_GET_LL(OPCODE)  ((OPCODE) & 0xFU)
#define C8_OPC_GET_NNN(OPCODE) ((OPCODE) & 0xFFFU)
#define C8_OPC_GET_NN(OPCODE)  ((OPCODE) & 0xFFU)
#define C8_OPC_GET_N(OPCODE)   ((OPCODE) & 0xFU)
#define C8_OPC_GET_X(OPCODE)   ((((OPCODE) & 0xF00U) >> 8U) & 0xFU)
#define C8_OPC_GET_Y(OPCODE)   ((((OPCODE) & 0xF0U) >> 4U) & 0xFU)

typedef struct {
	void          (*clr_scr)(void);
	void          (*pixel_set)(unsigned int x, unsigned int y, int on);
	int           (*pixel_get)(unsigned int x, unsigned int y);
	int           (*key_press)(unsigned char key);
	unsigned char (*get_key)(void);
	unsigned char (*random)(void);
} C8_CBacks;

typedef struct {
	C8_CBacks     cbs;
	unsigned int  stk[C8_EMU_STK_SIZE], ireg, pc;
	unsigned char vreg[C8_EMU_REG_SIZE], ram[C8_EMU_RAM_SIZE], dtm, stm, sp;
} C8_Emu;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

int c8_assem(char *buffer, unsigned char opcodes[2]);
int c8_disasm(const unsigned char opcodes[2], char *buffer);

int c8_emu_init(C8_Emu *emu, const unsigned char *prog, unsigned int length, const C8_CBacks *cbs);
int c8_emu_reset(C8_Emu *emu);
int c8_emu_step(C8_Emu *emu);
int c8_emu_timers(C8_Emu *emu);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* C8_H */
