#include "c8.h"

int
c8_emu_timers(C8_Emu *const emu)
{
	if (emu == NULL)
		return -C8_ERR_ARG_NULL;
	if (emu->dtm > 0)
		--emu->dtm;
	if (emu->stm > 0)
		--emu->stm;
	return C8_ERR_NONE;
}
