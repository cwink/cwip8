# CWip8

A Chip8 project that includes an assembler, disassembler, and emulator. I make
every attempt to follow the [suckless](https://suckless.org) philosophy and
style as close as possible.

## BNF

The grammar for the assembler/disassembler is as follows:

	<cmt> ::= <wsp> ";" <*> <eol>
	<ins> ::= <wsp> <cmd> <wsp> <eol>
	<eol> ::= "\n" || "\r\n"
	<cmd> ::= <add> | <and> | <cal> | <cls> | <drw> | <jmp> | <mov> |
	          <oor> | <ret> | <rnd> | <shl> | <shr> | <sie> | <sin> |
	          <skn> | <skp> | <sub> | <sun> | <sys> | <xor>
	<add> ::= "ADD" <wsp> <vrg> <wsp> "," <wsp> <byt> |
	          "ADD" <wsp> <vrg> <wsp> "," <wsp> <vrg> |
	          "ADD" <wsp> <irg> <wsp> "," <wsp> <vrg>
	<and> ::= "AND" <wsp> <vrg> <wsp> "," <wsp> <vrg>
	<cal> ::= "CAL" <wsp> <con>
	<cls> ::= "CLS"
	<drw> ::= "DRW" <wsp> <vrg> <wsp> "," <wsp> <vrg> <wsp> "," <wsp> <nib>
	<jmp> ::= "JMP" <wsp> <con> | "JMP" <wsp> "V0" <wsp> "," <wsp> <con>
	<mov> ::= "MOV" <wsp> <vrg> <wsp> "," <wsp> <byt> |
	          "MOV" <wsp> <vrg> <wsp> "," <wsp> <vrg> |
	          "MOV" <wsp> <irg> <wsp> "," <wsp> <con> |
	          "MOV" <wsp> <vrg> <wsp> "," <wsp> <dtm> |
	          "MOV" <wsp> <vrg> <wsp> "," <wsp> <key> |
	          "MOV" <wsp> <dtm> <wsp> "," <wsp> <vrg> |
	          "MOV" <wsp> <stm> <wsp> "," <wsp> <vrg> |
	          "MOV" <wsp> <fnt> <wsp> "," <wsp> <vrg> |
	          "MOV" <wsp> <bcd> <wsp> "," <wsp> <vrg> |
	          "MOV" <wsp> <ime> <wsp> "," <wsp> <vrg> |
	          "MOV" <wsp> <vrg> <wsp> "," <wsp> <ime>
	<oor> ::= "OOR" <wsp> <vrg> <wsp> "," <wsp> <vrg>
	<ret> ::= "RET"
	<rnd> ::= "RND" <wsp> <vrg> <wsp> "," <wsp> <byt>
	<shl> ::= "SHL" <wsp> <vrg>
	<shr> ::= "SHR" <wsp> <vrg>
	<sie> ::= "SIE" <wsp> <vrg> <wsp> "," <wsp> <byt> |
	          "SIE" <wsp> <vrg> <wsp> "," <wsp> <vrg>
	<sin> ::= "SIN" <wsp> <vrg> <wsp> "," <wsp> <byt> |
	          "SIN" <wsp> <vrg> <wsp> "," <wsp> <vrg>
	<skn> ::= "SKN" <wsp> <vrg>
	<skp> ::= "SKP" <wsp> <vrg>
	<sub> ::= "SUB" <wsp> <vrg> <wsp> "," <wsp> <vrg>
	<sun> ::= "SUN" <wsp> <vrg> <wsp> "," <wsp> <vrg>
	<sys> ::= "SYS" <wsp> <con>
	<xor> ::= "XOR" <wsp> <vrg> <wsp> "," <wsp> <vrg>
	<ime> ::= "[" <irg> "]"
	<irg> ::= "I"
	<vrg> ::= "V" <hex>
	<dtm> ::= "DT"
	<stm> ::= "ST"
	<key> ::= "K"
	<fnt> ::= "F"
	<bcd> ::= "B"
	<con> ::= <byt> | "0x" <hex> <hex> <hex>
	<byt> ::= <nib> | "0x" <hex> <hex>
	<nib> ::= "0x" <hex>
	<hex> ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" |
	          "9" | "A" | "B" | "C" | "D" | "E" | "F"
	<wsp> ::= " " | "\t" | <wsp>

## Links

The following are links that helped in learning about the Chip8 hardware:

* [https://en.wikipedia.org/wiki/CHIP-8](https://en.wikipedia.org/wiki/CHIP-8)
* [http://devernay.free.fr/hacks/chip8/C8TECH10.HTM](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
