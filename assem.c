#ifdef _WIN32
#	include <fcntl.h>
#	include <io.h>
#endif /* _WIN32 */

#include <stdio.h>
#include <stdlib.h>

#include "c8.h"

int
main(int argc, char *argv[])
{
	unsigned int b_len, l_num;
	int ch, ec;
	unsigned char opcodes[2];
	char buffer[64];
	b_len = 0;
	l_num = 1;
#ifdef _WIN32
	if (_setmode(_fileno(stdin), _O_BINARY) == -1) {
		fprintf(stderr, "ERROR: fialed to set stdin to binary mode.\n");
		return EXIT_FAILURE;
	}
#endif /* _WIN32 */
	while ((ch = fgetc(stdin)) != EOF) {
		if (ch == '\n' || ch == '\r') {
			if ((ch = fgetc(stdin)) != '\n' && ch != '\r')
				ungetc(ch, stdin);
			buffer[b_len] = '\0';
			if ((ec = c8_assem(buffer, opcodes)) < 0) {
				fprintf(stderr, "ERROR: error with code '%d' encountered on line '%u'\n", ec, l_num);
				return EXIT_FAILURE;
			}
			fwrite(opcodes, sizeof(unsigned char), 2, stdout);
			b_len = 0;
			++l_num;
			continue;
		}
		buffer[b_len++] = (char)ch;
	}
	if (ferror(stdin) != 0) {
		fprintf(stderr, "ERROR: failed to read data from stdin.\n");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
