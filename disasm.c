#include <stdio.h>
#include <stdlib.h>

#include "c8.h"

int
main(int argc, char *argv[])
{
	unsigned char opcodes[2];
	char buffer[64];
	int left, right;
#ifdef _WIN32
	if (_setmode(_fileno(stdin), _O_BINARY) == -1) {
		fprintf(stderr, "ERROR: fialed to set stdin to binary mode.\n");
		return EXIT_FAILURE;
	}
#endif /* _WIN32 */
	while ((left = fgetc(stdin)) != EOF && (right = fgetc(stdin)) != EOF) {
		opcodes[0] = left;
		opcodes[1] = right;
		if (c8_disasm(opcodes, buffer) < 0) {
			fprintf(stderr, "ERROR: invalid opcode '%.2x%.2x' encountered.\n", opcodes[0], opcodes[1]);
			return EXIT_FAILURE;
		}
		printf("%s\n", buffer);
	}
	if (ferror(stdin) != 0) {
		fprintf(stderr, "ERROR: failed to read data from stdin.\n");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
