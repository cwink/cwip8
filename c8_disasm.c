#include "c8.h"

int
c8_disasm(const unsigned char opcodes[2], char *const buffer)
{
	const unsigned int opcode = ((opcodes[0] << 8U) & 0xFF00U) | (opcodes[1] & 0xFFU);
	switch (C8_OPC_GET_HH(opcode)) {
	case 0x0:
		switch (C8_OPC_GET_L(opcode)) {
		case 0xE0:
			C8_SPRINTF(buffer, "%s", C8_SYM_CLS);
			break;
		case 0xEE:
			C8_SPRINTF(buffer, "%s", C8_SYM_RET);
			break;
		default:
			C8_SPRINTF(buffer, "%-4s %u", C8_SYM_SYS, C8_OPC_GET_NNN(opcode));
			break;
		}
		break;
	case 0x1:
		C8_SPRINTF(buffer, "%-4s %u", C8_SYM_JMP, C8_OPC_GET_NNN(opcode));
		break;
	case 0x2:
		C8_SPRINTF(buffer, "%-4s %u", C8_SYM_CALL, C8_OPC_GET_NNN(opcode));
		break;
	case 0x3:
		C8_SPRINTF(buffer, "%-4s %s%.1X, %u", C8_SYM_SE, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_OPC_GET_NN(opcode));
		break;
	case 0x4:
		C8_SPRINTF(buffer, "%-4s %s%.1X, %u", C8_SYM_SNE, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_OPC_GET_NN(opcode));
		break;
	case 0x5:
		if (C8_OPC_GET_LL(opcode) != 0x0) {
			return -C8_ERR_INV_OPC;
		}
		C8_SPRINTF(buffer, "%-4s %s%.1X, %s%.1X", C8_SYM_SE, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_VREG, C8_OPC_GET_Y(opcode));
		break;
	case 0x6:
		C8_SPRINTF(buffer, "%-4s %s%.1X, %u", C8_SYM_MOV, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_OPC_GET_NN(opcode));
		break;
	case 0x7:
		C8_SPRINTF(buffer, "%-4s %s%.1X, %u", C8_SYM_ADD, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_OPC_GET_NN(opcode));
		break;
	case 0x8:
		switch (C8_OPC_GET_LL(opcode)) {
		case 0x0:
			C8_SPRINTF(buffer, "%-4s %s%.1X, %s%.1X", C8_SYM_MOV, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_VREG, C8_OPC_GET_Y(opcode));
			break;
		case 0x1:
			C8_SPRINTF(buffer, "%-4s %s%.1X, %s%.1X", C8_SYM_OR, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_VREG, C8_OPC_GET_Y(opcode));
			break;
		case 0x2:
			C8_SPRINTF(buffer, "%-4s %s%.1X, %s%.1X", C8_SYM_AND, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_VREG, C8_OPC_GET_Y(opcode));
			break;
		case 0x3:
			C8_SPRINTF(buffer, "%-4s %s%.1X, %s%.1X", C8_SYM_XOR, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_VREG, C8_OPC_GET_Y(opcode));
			break;
		case 0x4:
			C8_SPRINTF(buffer, "%-4s %s%.1X, %s%.1X", C8_SYM_ADD, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_VREG, C8_OPC_GET_Y(opcode));
			break;
		case 0x5:
			C8_SPRINTF(buffer, "%-4s %s%.1X, %s%.1X", C8_SYM_SUB, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_VREG, C8_OPC_GET_Y(opcode));
			break;
		case 0x6:
			C8_SPRINTF(buffer, "%-4s %s%.1X", C8_SYM_SHR, C8_SYM_VREG, C8_OPC_GET_X(opcode));
			break;
		case 0x7:
			C8_SPRINTF(buffer, "%-4s %s%.1X, %s%.1X", C8_SYM_SUBN, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_VREG, C8_OPC_GET_Y(opcode));
			break;
		case 0xE:
			C8_SPRINTF(buffer, "%-4s %s%.1X", C8_SYM_SHL, C8_SYM_VREG, C8_OPC_GET_X(opcode));
			break;
		default:
			return -C8_ERR_INV_OPC;
		}
		break;
	case 0x9:
		if (C8_OPC_GET_LL(opcode) != 0x0) {
			return -C8_ERR_INV_OPC;
		}
		C8_SPRINTF(buffer, "%-4s %s%.1X, %s%.1X", C8_SYM_SNE, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_VREG, C8_OPC_GET_Y(opcode));
		break;
	case 0xA:
		C8_SPRINTF(buffer, "%-4s %s, %u", C8_SYM_MOV, C8_SYM_IREG, C8_OPC_GET_NNN(opcode));
		break;
	case 0xB:
		C8_SPRINTF(buffer, "%-4s %s0, %u", C8_SYM_JMP, C8_SYM_VREG, C8_OPC_GET_NNN(opcode));
		break;
	case 0xC:
		C8_SPRINTF(buffer, "%-4s %s%.1X, %u", C8_SYM_RND, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_OPC_GET_NN(opcode));
		break;
	case 0xD:
		C8_SPRINTF(buffer, "%-4s %s%.1X, %s%.1X, %u", C8_SYM_DRW, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_VREG, C8_OPC_GET_Y(opcode), C8_OPC_GET_N(opcode));
		break;
	case 0xE:
		switch (C8_OPC_GET_L(opcode)) {
		case 0x9E:
			C8_SPRINTF(buffer, "%-4s %s%.1X", C8_SYM_SKP, C8_SYM_VREG, C8_OPC_GET_X(opcode));
			break;
		case 0xA1:
			C8_SPRINTF(buffer, "%-4s %s%.1X", C8_SYM_SKNP, C8_SYM_VREG, C8_OPC_GET_X(opcode));
			break;
		default:
			return -C8_ERR_INV_OPC;
		}
		break;
	case 0xF:
		switch (C8_OPC_GET_L(opcode)) {
		case 0x07:
			C8_SPRINTF(buffer, "%-4s %s%.1X, %s", C8_SYM_MOV, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_DTM);
			break;
		case 0x0A:
			C8_SPRINTF(buffer, "%-4s %s%.1X, %s", C8_SYM_MOV, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_KEY);
			break;
		case 0x15:
			C8_SPRINTF(buffer, "%-4s %s, %s%.1X", C8_SYM_MOV, C8_SYM_DTM, C8_SYM_VREG, C8_OPC_GET_X(opcode));
			break;
		case 0x18:
			C8_SPRINTF(buffer, "%-4s %s, %s%.1X", C8_SYM_MOV, C8_SYM_STM, C8_SYM_VREG, C8_OPC_GET_X(opcode));
			break;
		case 0x1E:
			C8_SPRINTF(buffer, "%-4s %s, %s%.1X", C8_SYM_ADD, C8_SYM_IREG, C8_SYM_VREG, C8_OPC_GET_X(opcode));
			break;
		case 0x29:
			C8_SPRINTF(buffer, "%-4s %s, %s%.1X", C8_SYM_MOV, C8_SYM_FONT, C8_SYM_VREG, C8_OPC_GET_X(opcode));
			break;
		case 0x33:
			C8_SPRINTF(buffer, "%-4s %s, %s%.1X", C8_SYM_MOV, C8_SYM_BCD, C8_SYM_VREG, C8_OPC_GET_X(opcode));
			break;
		case 0x55:
			C8_SPRINTF(buffer, "%-4s %s, %s%.1X", C8_SYM_MOV, C8_SYM_IMEM, C8_SYM_VREG, C8_OPC_GET_X(opcode));
			break;
		case 0x65:
			C8_SPRINTF(buffer, "%-4s %s%.1X, %s", C8_SYM_MOV, C8_SYM_VREG, C8_OPC_GET_X(opcode), C8_SYM_IMEM);
			break;
		default:
			return -C8_ERR_INV_OPC;
		}
		break;
	default:
		return -C8_ERR_INV_OPC;
	}
	return C8_ERR_NONE;
}
